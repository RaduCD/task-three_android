package com.example.task_three_android

import android.app.AlarmManager
import android.app.DatePickerDialog
import android.app.PendingIntent
import android.app.TimePickerDialog
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.AlarmClock
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.TimePicker
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.getSystemService
import java.lang.StringBuilder
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*
import javax.xml.datatype.DatatypeConstants
import javax.xml.datatype.DatatypeConstants.MONTHS

/**
 * A simple [Fragment] subclass.
 */
class FirstFragment : Fragment() {

    var buttonGetTimePicker: Button? = null
    var buttonGetDatePicker: Button? = null
    var buttonSetAlarm: Button? = null
    var textViewDisplayTimePicker: TextView? = null
    var textViewDisplayDatePicker: TextView? = null
    var mHour: Int? = null
    var mMinute: Int? = null
    var mDay: Int? = null
    var mMonth: Int? = null
    var mYear: Int? = null

    var alarmManager: AlarmManager? = null
    var pendingIntent: PendingIntent? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonGetTimePicker = view.findViewById(R.id.btnGetTimePicker) as Button
        buttonGetDatePicker = view.findViewById(R.id.btnGetDatePicker) as Button
        buttonSetAlarm= view.findViewById(R.id.btnSetAlarm) as Button
        textViewDisplayTimePicker  = view.findViewById(R.id.tvRealTimePicker) as TextView
        textViewDisplayDatePicker  = view.findViewById(R.id.tvRealDatePicker) as TextView


        alarmManager = activity!!.getSystemService(ALARM_SERVICE) as AlarmManager

        buttonGetTimePicker?.setOnClickListener {
            setTimeDialog()
        }

        buttonGetDatePicker?.setOnClickListener {
             setDateDialog()
        }

        buttonSetAlarm?.setOnClickListener {
            setAlarm()
        }

    }

    private fun setDateDialog(){
        val calendar = Calendar.getInstance()
        val dateSetListener  = DatePickerDialog.OnDateSetListener { datePicker, year, monthOfYear, dayOfMonth ->

            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            mDay = dayOfMonth
            calendar.set(Calendar.MONTH, monthOfYear)
            mMonth = monthOfYear
            calendar.set(Calendar.YEAR, year)
            mYear = year
            var dateText :StringBuilder = StringBuilder()
            dateText?.append("The date is: ")
            dateText?.append(dayOfMonth.toString())
            dateText?.append('/')
            dateText?.append((monthOfYear + 1).toString())
            dateText?.append('/')
            dateText?.append(year)

            textViewDisplayDatePicker?.text = dateText
        }

        context?.let { DatePickerDialog(it, dateSetListener, calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.MONTH), calendar.get(Calendar.YEAR)).show() }
    }
    private fun setTimeDialog(){
        val calendar = Calendar.getInstance()
        val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
            calendar.set(Calendar.HOUR_OF_DAY, hour)
            mHour = hour
            calendar.set(Calendar.MINUTE, minute)
            mMinute = minute

            var timeText :StringBuilder = StringBuilder()
            timeText?.append("The time is: ")
            timeText?.append(SimpleDateFormat("HH:mm").format(calendar.time).toString())
            textViewDisplayTimePicker?.text =  timeText
        }
        TimePickerDialog(context, timeSetListener, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true).show()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun setAlarm(){
        val intent = Intent(AlarmClock.ACTION_SET_ALARM)
        intent.putExtra(AlarmClock.EXTRA_MESSAGE, "Alarm!!")
        intent.putExtra(AlarmClock.EXTRA_MINUTES, mMinute);
        intent.putExtra(AlarmClock.EXTRA_HOUR, mHour);

        var date: Calendar = Calendar.getInstance()
        if(mYear!=null && mMonth!=null && mDay!=null)
        date.set(mYear!!, mMonth!!, mDay!!)


       intent.putExtra("date", date)

       // intent.putExtra(DateTimeFormatter.ISO_LOCAL_DATE, mYear, mMonth, mDay);
        startActivity(intent)
    }

    /*fun onToggleClicked(view: View) {

        startAlarm(view)
    }

    private fun startAlarm(view: View) {
        if ((view as ToggleButton).isChecked) {
            val calendar = Calendar.getInstance()
            calendar.set(Calendar.HOUR_OF_DAY, alarmTimePicker!!.currentHour)
            calendar.set(Calendar.MINUTE, alarmTimePicker!!.currentMinute)
            val intent = Intent(this, AlarmReceiver::class.java)
            pendingIntent = PendingIntent.getBroadcast(this, 0, intent, 0)

            var time = calendar.timeInMillis - calendar.timeInMillis % 60000

            if (System.currentTimeMillis() > time) {
                if (Calendar.AM_PM === 0)
                    time += 1000 * 60 * 60 * 12
                else
                    time += time + 1000 * 60 * 60 * 24
            }
            /* For Repeating Alarm set time intervals as 10000 like below lines */
            // alarmManager!!.setRepeating(AlarmManager.RTC_WAKEUP, time, 10000, pendingIntent)

            alarmManager!!.set(AlarmManager.RTC, time, pendingIntent);
            Toast.makeText(this, "ALARM ON", Toast.LENGTH_SHORT).show()
        } else {
            alarmManager!!.cancel(pendingIntent)
            Toast.makeText(this, "ALARM OFF", Toast.LENGTH_SHORT).show()
        }
    }

     */
}







