package com.example.task_three_android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        goToFirstFragment()
    }

    private fun goToFirstFragment(){
        val firstFragment = FirstFragment()

        supportFragmentManager.beginTransaction().add(R.id.fragmentContainer, firstFragment)
            .addToBackStack("FIRST FRAGMENT").commit()
    }
}
